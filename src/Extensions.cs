﻿using System.Runtime.CompilerServices;

namespace LearningTheControls;

public static class Extensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Not(this bool b) => !b;
}