using Godot;

namespace LearningTheControls.Player;

public partial class Player: CharacterBody3D
{
	[Export] public float Speed { get; set; } = 20f;
	[Export] public float TurnSpeed { get; set; } = 30f;
	[Export] public float Friction { get; set; } = 0.95f;
	[Export] public float JumpPower { get; set; } = 4.5f;
	[Export] public float CoyoteTime { get; set; } = 0.1f;
	[Export] public float JumpBuffer { get; set; } = 0.1f;
	[Export] public float MaxFallSpeed { get; set; } = -200f;

	private bool _holdingJump;
	private bool _wasOnFloor;

	private Timer _coyoteTimer = new Timer();
	private Timer _jumpBufferTimer = new Timer();

	private const float Gravity = -2f;
	private const float GravityCancelFactor = 2f;
	private float CurrentGravity => _holdingJump ? Gravity : Gravity * GravityCancelFactor;

	private static bool PlayerIsInputting =>
		Input.IsActionPressed("move_backward")
		|| Input.IsActionPressed("move_forward")
		|| Input.IsActionPressed("move_left")
		|| Input.IsActionPressed("move_right");
	
	private static bool PlayerIsTurning =>
		Input.IsActionPressed("turn_left")
		|| Input.IsActionPressed("turn_right");

	private bool CanJump =>
		IsOnFloor() || _coyoteTimer.IsStopped().Not();

	public override void _Ready()
	{
		_configureTimers();
	}

	public override void _Process(double delta)
	{
		
	}
	public override void _Input(InputEvent inputEvent)
	{
		if (inputEvent.IsActionPressed("jump"))
		{
			if(CanJump) _jump();
			else _jumpBufferTimer.Start();
		}
		if (inputEvent.IsActionReleased("jump")) _holdingJump = false;

	}

	private void _configureTimers()
	{
		_coyoteTimer.Autostart = false;
		_coyoteTimer.OneShot = true;
		_coyoteTimer.WaitTime = CoyoteTime;
		AddChild(_coyoteTimer);
		
		_jumpBufferTimer.Autostart = false;
		_jumpBufferTimer.OneShot = true;
		_jumpBufferTimer.WaitTime = JumpBuffer;
		AddChild(_jumpBufferTimer);
	}

	private void _jump()
	{
		Velocity += Vector3.Up * JumpPower;
		_holdingJump = true;
	}
	private float _getTurningForce() => Input.GetAxis("turn_right", "turn_left") * TurnSpeed;

	public override void _PhysicsProcess(double delta)
	{
		Vector3 velocity = Velocity;
		if (_wasOnFloor && IsOnFloor().Not())
		{
			_coyoteTimer.Start();
		}
		else if (_wasOnFloor.Not() && IsOnFloor() && _jumpBufferTimer.IsStopped().Not())
		{
			_jump();
		}
		_wasOnFloor = IsOnFloor();
		var inputs = _getInputs();
		var direction = (Transform.Basis * new Vector3(inputs.X, 0, inputs.Y)).Normalized();

		if (inputs != Vector2.Zero)
		{
			velocity.X = direction.X * Speed;
			velocity.Z = direction.Z * Speed;
		}
		else
		{
			velocity.X = Velocity.X * Friction;
			velocity.Z = Velocity.Z * Friction;
		}

		if (PlayerIsTurning)
		{
			Rotate(Vector3.Up, _getTurningForce() * (float)delta);
		}		
		Velocity = new Vector3(velocity.X, Mathf.Max(Velocity.Y + CurrentGravity, MaxFallSpeed), velocity.Z);
		
		MoveAndSlide();
	}

	private Vector2 _getInputs() => 
		Input.GetVector(
			"move_left",
			"move_right", 
			"move_forward",
			"move_backward"
		);
}
