# Learning the Controls

This is a simple game for toddlers, young kids, or anyone else who needs to learn how to use a controller in a safe, sandboxed environment. 

It is designed to encourage the player to move around and get used to the feeling of a controller in their hands.
